export const once = (element, eventName, callback) => {
    const handleEvent = (event) => {
	element.removeEventListener(eventName, handleEvent)
	callback(event)
    }
    element.addEventListener(eventName, handleEvent)
}

export const on = (element, eventName, callback) => {
    const handleEvent = (event) => {
	callback(event)
    }
    element.addEventListener(eventName, handleEvent)
    return () => element.removeEventListener(eventName, handleEvent)
}
